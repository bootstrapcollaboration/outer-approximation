\documentclass[11pt]{article}
\usepackage{geometry}
\usepackage{hyperref}

\geometry{letterpaper}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{color}
\usepackage{dsdshorthand}
\usepackage{subcaption}

\begin{document}

\section{Outer approximation methods}

In our discussions at Stony Brook, Pablo Parrilo emphasized the virtues of ``outer approximation" methods for optimization. These methods have the possible advantages:

\begin{itemize}
\item They work directly with the functional, without any additional auxiliary variables (i.e.\ matrices encoding positive semidefiniteness). Thus, it is possible that they avoid ill-conditioning problems that arise in our current use of semidefinite programming.

\item They don't rely on polynomial approximations, and thus would better handle different types of functionals, including possibly analytic functionals.

\item They open up lots of interesting opportunities for hot-starting, including starting from the MFT spectrum, combining spectra from nearby points to guess good starting points, etc.
\end{itemize}

\subsection{Basic algorithm}

Suppose we would like to find $y\in \R^{N}$ such that
\be
Q_j(y;x) \equiv \sum_{n=1}^N y_n M^n_j(x) \succeq 0,
\ee
for $j=1,\dots,J$. Here, each $M^n_j(x)$ is a (real, symmetric) matrix-valued function of $x$ (we are suppressing its matrix indices) that is not necessarily a polynomial of $x$. The index $j$ roughly runs over spins and global symmetry representations, and the index $n$ labels components in some basis of functionals. $y\in \R$ is a functional that we would like to find, and let us impose some normalization condition $n\. y=1$.

Let us start with an initial guess $y^{(0)}$. We form the matrix functions $Q_j(y^{(0)};x)$. Presumably, they are not all positive-definite. We find a bunch of values $\{(j_1,x_1),\dots,(j_K,x_K)\}$ where $Q_{j_k}(y^{(0)};x_k)$ has negative eigenvalues. We impose the positivity constraints
\be
\sum_{n=1}^N y_n M^n_{j_k}(x_k) \succeq 0 \qquad (k=1,\dots,K)
\ee
and use SDPB to find a new functional $y^{(1)}$ satisfying these constraints. If we now form $Q_j(y^{(1)};x)$, it will be better than $Q_j(y^{(0)};x)$ because it is now positive-semidefinite in more places. However, it will not necessarily be positive-semidefinite everywhere. Thus, we search for values $(j,x)$ where it has negative eigenvalues and repeat.

Eventually, we will have a long list of values $(j_i,x_i)$ that will be sufficient to constrain the functional $y$ to be positive within some threshold. At this point we stop.

\subsection{Choosing and finding the $(j_i,x_i)$}

A natural way to choose the negative points is to pick them to be negative local minima of the eigenvalues of $Q_j(y;x)$. One way to do this is by a modification of the algorithm from {\tt https://arxiv.org/abs/1403.4545}. Suppose we are given a matrix function $Q(x)$ and we would like to find its negative local minimum eigenvalues. Our first task is to approximate $Q(x)$ very well. We take the range that $x$ lives in and split it up into finite intervals $[a_1,b_1],[a_2,b_2],\dots$. To do this, we must compactify the range somehow, perhaps by a fractional linear transformation (?). Within an interval $[a,b]$, we build a cubic spline interpolation of $Q(x)$ by sampling it at the endpoints $[a,b]$, together with two points in the middle. We test the quality of this interpolation by comparing to the interpolation at other points inside the interval. If the interpolation is bad, we cut up the interval and recurse. If the interpolation is good, we perform a computation to find the minimum eigenvalue within the interval using the cubic spline.

Parillo mentioned an eigenvalue computation for determining the minimum eigenvalue of a polynomial matrix. I have not been able to reproduce this computation for a general matrix $P(x)$. However, for a $1\x 1$ matrix that is cubic in $x$, it is trivial to find the minimum. A variant of this algorithm in the $1\x1$ case has already been implemented at least twice -- once in {\tt sip-solver} and once in code written by Slava and Sheer.

\subsubsection{Differences from the algorithm from {\tt https://arxiv.org/abs/1403.4545}}

The logic of the $1\x 1$ algorithm in {\tt https://arxiv.org/abs/1403.4545} (see section 6.2.4) was somewhat different.
The goal of subdivision into intervals $[a_i,b_i]$ (on each of which $Q'(x)$ was approximated by a quadratic polynomila) was to make sure that there was at most one root of $Q'(x)$ on each interval. The actual roots were found via a variant of Newton's method applied directly to $Q'(x)$, not to the quadratic polynomial approximation. The roots were searched for only on those intervals where $Q'(a_i)<0$, $Q'(b_i)>0$, so that they correspond to local minima.

It is reported in {\tt https://arxiv.org/abs/1403.4545} that applying Newton's method was a computationally subdominant, to the subdivision, part of the algorithm.


\subsection{Throwing away or keeping constraints}

In addition to adding new constraints at each step, we also have the option of throwing away unnecessary constraints. This is an optimization that we may or may not want to use -- it will take some testing.

\subsection{Types of constraints}

Instead of imposing that $Q_{j_i}(y;x_i)$ is positive-semidefinite, one can impose that it is positive along a single vector (i.e. the most negative eigenvector), or as Parillo suggested, impose that it is positive in the subspace in which it previously contained negative eigenvalues. This is also something to test.

\subsection{Hot-starting}

If we can hot start each time we add constraints, it would improve the runtime significantly. I believe Ning has experimented with this.

\section{Attempt at a simplest algorithm, to code up and test}

{\bf Inputs:} 
\begin{enumerate}
	\item a real, symmetric matrix valued function $Q(x)$ on $x\in[A,\infty)$ with $Q(\infty)=0$.

\item a list of points $x_i$ for which it's known that $Q(x_i)\succeq 0$.
\end{enumerate}
Let $\lambda(x)$ denote the smallest eigenvalue of $Q(x)$.

{\bf Output:} A point $x_*$ at which $\lambda(x)$ attains its minimum on $[A,\infty)$.

Permitted operations:

\begin{enumerate}
	\item evaluate $Q(x)$ for a given $x$.\footnote{In our test this function will be given as a sum
	$Q(x) = r_*^x \sum_{n=1}^N y_n M^n(x)$ where $n$ numbers derivatives, $y_n$ is a list of reals, and $M^n(x)$ are polynomial approximations to derivatives of $F$ matrix functions, and $r_* \approx 0.17$. The role of factor $r_*^x$ is to provide normalization favoring low scaling dimensions. This is the same normalization used in \cite{cmin} and it worked well there. The routine evaluating $Q(x)$ will know these details, but they may remain opaque to the rest of the algorithm.}
	
	\item compute $\lambda(x)$ at a given $x$. In fact $Q(x)$ will not be used directly, only $\lambda(x)$.
	
\end{enumerate}
	
	Although we are interested in $[A,\infty)$, as a first try let us consider $[A,B]$ with very large $B$, as was done in \cite{cmin}.
	
	Also I am not sure how to use efficiently information in Input 2, so I will not use it for now.

The algorithm will have two phases:

Phase 1: subdivide interval $[A,B]$ into``good'' intervals, in the sense defined below.
The goodness condition heuristically guarantees that $\lambda(x)$ has at most one local minimum on each good interval.

Phase 2. Minimize $\lambda(x)$ individually on each good interval (or rather on those of them where a local minimum may exist according to some heuristics).
Choose $x_*$ which give the smallest $\lambda_1$ from these individual minimization.


Phase 1 detailed description: 
The state of the algorithm will be defined by decomposition $[A,B]=\cup_{I\in L} I$ where $L$ is a finite set of ``good'' nonoverlapping intervals covering $[A,B]$. At the first step we have only one interval in this set: $L=[A,B]$, marked as ``bad''. 

We will do a cycle over all bad intervals.

Take a bad interval $I=[a,c]$. We will bisect it in golden ratio proportion until it becomes small and good, acting as follows. (Other proportions are possible, see below.) Evaluate $\lambda(x)$ at 3 points 
\be
a<b= (1-w)a+w c< c
\ee
where $w\approx 0.38197$ is the golden ratio. Construct quadratic polynomial $\hat\lambda(x)$ which agrees with $\lambda(x)$ at these 3 points.
Compare $\hat\lambda(x)$ with $\lambda(x)$ at $b'=a w+c (1-w) c$ by computing 
\be
\frac {\hat\lambda- \lambda}{|\hat\lambda| +|\lambda|}\quad \text{at $x=b'$}
\ee
If this is less than parameter $\epsilon=0.05$ (by analogy with \cite{cmin}) then we declare that the interval is good. The meaning of this criterion is that we believe, heuristically, that the shape of $\lambda(x)$ on this interval is well approximated by the quadratic approximation $\hat\lambda(x)$.

Otherwise we split the interval into $[a,b]$ and $[b,c]$ and repeat. Notice that evaluation of $\lambda(x)$ at $b$ and $b'$ will be recycled on subsequent steps.

A version of the algorithm without golden ratio:

Take a bad interval $I=[a,c]$. Evaluate $\lambda(x)$ at 5 equally spaced points:
\be
a, b_1,b_2,b_3, c
\ee
($b_2$ is the midpoint of $[a,c]$, and $b_1,b_3$ are midpoints of $[a,b_2]$ and $[b_2,c]$)
Compute quadratic approximation $\hat\lambda(x)$ to $\lambda(x)$ using points $a,b_2,c$ and compare its value to the true $\lambda(x)$ using the quantity
\be
\frac {\hat\lambda- \lambda}{|\hat\lambda| +|\lambda|}\quad \text{at $x=b_1,b_3$}
\ee
If this is less than parameter $\epsilon=0.05$ at both these points, then we declare that the interval is good. 

Otherwise we split the interval into $[a,b_2]$ and $[b_2,c]$ and repeat. Evaluations of $\lambda(x)$ at $b_1$ and $b_3$ will be recycled on the subsequent step.  


Phase 2: The above checks provide assurance that on each good interval $[a,c]$ the shape of $\lambda(x)$ is well approximated by the quadratic approximation $\hat\lambda(x)$. In particular there can be at most one local minimum of $\lambda(x)$ in the interior of $[a,c]$. At this point we forget about $\hat\lambda(x)$ and work with $\lambda(x)$. 

Notice also that at this stage we have computed $\lambda(x)$ at three points $a<b<c$ (perhaps at more interior points depending which Phase 1 version is used). If $\lambda(b)<\min (\lambda(a),\lambda(c))$ we say that the minimum is bracketed (This is the definition from Numerical Recipes, Chapter 10). 

If $\lambda(b) > \max (\lambda(a),\lambda(c))$ then the interval heuristically contains a local maximum, not local minimum and can be discarded from further consideration.

For other orderings of $\lambda(b)$ w.r.t. $\lambda(a),\lambda(c)$ the interval may contain a local minimum 
in the interior, or the function might be monotonic on this interval. More checks are required.

On good intervals where the minimum is bracketed we can find the location of the minimum using e.g.~Brent's method (see Numerical recipes Ch.10) based on parabolic interpolation. This method uses function values (but not derivatives) and converges superexponentially.

Once we so found the local minimum on every good interval containing a local minimum in its interior, we compare these minima and find the global minimum.

\vskip .2cm

An alternative for the transition between phase 1 and 2 is the following. Let us assume that the piecewise parabolic approximation of $\lambda(x)$ in phase 1 is reliable. Now for each interval we have evaluated $\lambda(x)$ at five points $p_i$: two endpoints and three intermediate points. The intermediate points $p_2$, $p_3$ and $p_4$ yield a natural split of the interval into four subintervals. If $\lambda(x)$ contains a minimum in the second or third subinterval then, by our reliability assumption, it should be bracketed. A problem can therefore only arise if $\lambda(x)$ contains a minimum in the first or fourth subinterval and it is not bracketed, so for example if $\lambda(p_3) > \lambda(p_4) > \lambda(p_5)$. This problem can be mitigated by considering the next interval, since presumably $\lambda(p_5)$ of the first interval is now lower than $\lambda(p_2)$ of the next interval (see below if this is not the case). By this logic, it is natural to just throw all points $p_i$ of all the intervals together and look for bracketed triplets. This is likely to work better than considering each interval in separation.

There are two potential problems: first $\lambda(p_2)$ of the next interval could actually be lower than $\lambda(p_5)$ of the previous interval. This however would mean, again by the reliability assumption, that the next interval has a local maximum in the first subinterval. The whole function, with a minimum shortly followed by a maximum, now looks more like a cubic than two parabolas, and we may take this as an indication that such a partitioning is unlikely to be reliable. The second issue arises at the endpoints where there is no next interval to check. The `throw everything together' idea ignores this subtlety. To mitigate this we could either use the parabolic approximation to estimate whether an unbracketed local minimum occurs close to an endpoint, or we can evaluate the function at a point outside the original interval $[A,B]$ (where it may not be defined).

\begin{thebibliography}{99}
	\bibitem{cmin} 
	S.~El-Showk, M.~F.~Paulos, D.~Poland, S.~Rychkov, D.~Simmons-Duffin and A.~Vichi,
	``Solving the 3d Ising Model with the Conformal Bootstrap II. c-Minimization and Precise Critical Exponents,''
	J.\ Stat.\ Phys.\  {\bf 157}, 869 (2014)
	doi:10.1007/s10955-014-1042-7
	[arXiv:1403.4545 [hep-th]].
	%%CITATION = doi:10.1007/s10955-014-1042-7;%%
	%264 citations counted in INSPIRE as of 20 Nov 2019
	\end{thebibliography}
\end{document}
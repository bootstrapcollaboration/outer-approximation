#include <boost/math/tools/minima.hpp>

template <class T>
struct PosVal { T x; T f; };

template <class T>
using Partition = std::array<PosVal<T>, 5>;

// used for debugging
template <class T>
std::ostream& operator<< (std::ostream& o, const PosVal<T>& a)
{
    o << " [x: " << a.x << ", f: " << a.f << "] ";
    return o;
}

template <class T, class F>
Partition<T> initialpartition(F f, T min, T max)
{
  PosVal<T> p0, p1, p2, p3, p4;
  p0.x = min;
  p4.x = max;
  p2.x = (p0.x + p4.x)/2;
  p1.x = (p0.x + p2.x)/2;
  p3.x = (p2.x + p4.x)/2;
  p0.f = f(p0.x);
  p1.f = f(p1.x);
  p2.f = f(p2.x);
  p3.f = f(p3.x);
  p4.f = f(p4.x);
  return Partition<T>({p0, p1, p2, p3, p4});
}

template <class T>
T quadraticfitval(T x, PosVal<T> p1, PosVal<T> p2, PosVal<T> p3)
{
  return
      p1.f * (x - p2.x) * (x - p3.x) / (p1.x - p2.x) / (p1.x - p3.x)
    + p2.f * (x - p3.x) * (x - p1.x) / (p2.x - p3.x) / (p2.x - p1.x)
    + p3.f * (x - p1.x) * (x - p2.x) / (p3.x - p1.x) / (p3.x - p2.x);
}

template <class T, class F>
std::vector<Partition<T>> getPartitions(F f, Partition<T> p)
{
    // TODO remove hard coded parameter
    T eps = 0.05;

    // compute quadratic approximation to intermediate points
    auto p1approx = quadraticfitval(p[1].x, p[0], p[2], p[4]);
    auto p3approx = quadraticfitval(p[3].x, p[0], p[2], p[4]);
    auto p1err = abs(p1approx - p[1].f) / (abs(p1approx) + abs(p[1].f) + eps);
    auto p3err = abs(p3approx - p[3].f) / (abs(p3approx) + abs(p[3].f) + eps);

    if (p1err < eps && p3err < eps)
    {
      return std::vector<Partition<T>>({p});
    }
    else
    {
      PosVal<T> p01, p12, p23, p34;
      p01.x = (p[0].x + p[1].x)/2;
      p01.f = f(p01.x);
      p12.x = (p[1].x + p[2].x)/2;
      p12.f = f(p12.x);
      p23.x = (p[2].x + p[3].x)/2;
      p23.f = f(p23.x);
      p34.x = (p[3].x + p[4].x)/2;
      p34.f = f(p34.x);
      Partition<T> pleft = {p[0], p01, p[1], p12, p[2]};
      Partition<T> pright = {p[2], p23, p[3], p34, p[4]};
      // debug: terminate after one step
      // std::vector<Partition> leftps = {pleft};
      // std::vector<Partition> rightps = {pright};
      auto leftps = getPartitions(f, pleft);
      auto rightps = getPartitions(f, pright);
      // concatenate vectors
      leftps.insert(leftps.end(), rightps.begin(), rightps.end());
      return leftps;
    }
}

template <class F, class T>
std::vector<std::pair<T,T>> getMinima(F f, T min, T max, int bits)
{
  // create partitions
  auto ip = initialpartition(f, min, max);
  auto parts = getPartitions(f, ip);

  // create and populate array with all PosVals in parts
  std::vector<PosVal<T>> allposvals;
  allposvals.push_back(parts[0][0]);
  for (const auto part : parts)
  {
    // debug
    // std::cout << "Partition: " << std::endl;
    // for (const auto pv : part)
    // {
    //    std::cout << pv << std::endl;
    // }

    // omit zeroth element because it's duplicated in the next partition
    allposvals.push_back(part[1]);
    allposvals.push_back(part[2]);
    allposvals.push_back(part[3]);
    allposvals.push_back(part[4]);
  }

  // find minima for bracketed triplets
  std::vector<std::pair<T, T>> mins;
  for (size_t i = 0; i < allposvals.size() - 2; i++)
  {
    auto p0 = allposvals[i];
    auto p1 = allposvals[i+1];
    auto p2 = allposvals[i+2];
    if (p1.f < p0.f && p1.f < p2.f)
    {
      // debug
      // std::cout << "Found a bracket: " << std::endl;
      // for (const auto pv : {p0, p1, p2})
      // {
      //    std::cout << pv << std::endl;
      // }
      auto localmin = boost::math::tools::brent_find_minima(f, p0.x, p2.x, bits);
      mins.push_back(localmin);
      // debug
      // std::cout << "Found local minimum at x = ";
      // std::cout << localmin.first;
      // std::cout << " with f(" << localmin.first << ") = " << localmin.second;
      // std::cout << std::endl;
    }
  }
  return mins;
}

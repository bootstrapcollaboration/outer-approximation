#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <istream>
#include <list>

#include "DerivativeTable.hxx"

#include "Bigfloat.hxx"
#include "Polynomial.hxx"

std::string get_file_contents(const boost::filesystem::path &filename) {
  std::ifstream in(filename.string(), std::ios::in | std::ios::binary);
  if (in) {
    std::string contents;
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], contents.size());
    in.close();
    return (contents);
  }
  throw std::runtime_error("Couldn't open file " + filename.string() +
                           " for reading.");
}

Polynomial<Bigfloat> parse_polynomial(std::string &data, size_t begin,
                                      size_t end);

DerivativeTable::DerivativeTable(const boost::filesystem::path &filename) {
  std::string file_contents(get_file_contents(filename));

  std::map<
      std::string,
      std::list<std::pair<std::pair<int64_t, int64_t>, Polynomial<Bigfloat>>>>
      data;

  try {
    std::list<std::pair<size_t, size_t>> entries;

    size_t token_start(0), token_end(0);
    int64_t square_braket_level(0);
    for (size_t i = 0; i < file_contents.length(); ++i) {
      if (file_contents.at(i) == '[')
        ++square_braket_level;
      if (file_contents.at(i) == ']')
        --square_braket_level;
      if (file_contents.at(i) == '{')
        token_start = i + 1;

      if (file_contents.at(i) == '}' ||
          (file_contents.at(i) == ',' && square_braket_level == 0)) {
        token_end = i - 1;
        entries.emplace_back(std::make_pair(token_start, token_end));
        token_start = i + 1;
        token_end = token_start;
      }
    }

    for (const auto &entry : entries) {
      size_t name_start(entry.first), name_end(0);
      while (file_contents.at(name_start) == ' ' ||
             file_contents.at(name_start) == '\n')
        ++name_start;
      name_end = name_start;
      while (file_contents.at(name_end) != '[')
        ++name_end;

      std::string name =
          file_contents.substr(name_start, name_end - name_start);

      size_t int_begin = name_end;
      while (!std::isdigit(file_contents.at(int_begin)))
        ++int_begin;
      size_t int_end = int_begin;
      while (std::isdigit(file_contents.at(int_end)))
        ++int_end;

      int64_t m = boost::lexical_cast<int64_t>(
          file_contents.c_str() + int_begin, int_end - int_begin);

      int_begin = int_end + 1;
      while (!std::isdigit(file_contents.at(int_begin)))
        ++int_begin;
      int_end = int_begin;
      while (std::isdigit(file_contents.at(int_end)))
        ++int_end;

      int64_t n = boost::lexical_cast<int64_t>(
          file_contents.c_str() + int_begin, int_end - int_begin);

      size_t poly_begin = int_end;
      while (file_contents.at(poly_begin) != '>')
        ++poly_begin;
      data[name].emplace_back(
          std::make_pair(m, n),
          ::parse_polynomial(file_contents, poly_begin + 1, entry.second));
    }

    // From here on we assume that we work with zzbDeriv tables
    auto &zzbTable = data["zzbDeriv"];

    Lambda = 0;
    for (auto &entry : zzbTable) {
      if (Lambda < static_cast<size_t>(entry.first.first + entry.first.second))
        Lambda = entry.first.first + entry.first.second;
    }

    derivatives.resize(Lambda + 1);
    for (size_t m = 0; m <= Lambda; ++m)
      derivatives.at(m).resize(std::min(Lambda - m + 1, m + 1));

    for (auto &entry : zzbTable) {
      size_t m = entry.first.first;
      size_t n = entry.first.second;

      derivatives.at(m).at(n) = std::move(entry.second);
    }

  } catch (std::exception e) {
    throw std::runtime_error("Couldn't parse derivative table from " +
                             filename.string() +
                             "\n Original exception: " + e.what());
  }
}

Polynomial<Bigfloat> &DerivativeTable::derivative(size_t m, size_t n) {
  if (m >= n)
    return derivatives.at(m).at(n);
  else
    return derivatives.at(n).at(m);
}

// return start and end of first and last non-space chars of the first token at
// or after pos
std::pair<size_t, size_t> find_next_token(const std::string &data, size_t pos,
                                          size_t end) {
  size_t first;
  while (pos < end + 1 && std::isspace(data.at(pos)))
    ++pos;
  if (pos >= end + 1)
    return std::make_pair(end + 1, end + 1);

  first = pos;
  char c = data.at(pos);

  if (std::isdigit(c) || c == '.') {
    // current token is a number
    while (pos < end + 1 && (std::isdigit(c) || c == '.')) {
      ++pos;
      c = data.at(pos);
    }
    return std::make_pair(first, pos - 1);
  }
  if (c == 'x' || c == '+' || c == '-' || c == '^' || c == '*') {
    return std::make_pair(first, first);
  }

  throw std::runtime_error(
      "Unrecognzied token in polynomial starting at " +
      data.substr(pos, std::min(static_cast<size_t>(10), end - pos + 1)));
}

Polynomial<Bigfloat> parse_polynomial(std::string &data, size_t begin,
                                      size_t end) {
  std::list<Bigfloat> coefficients;
  std::pair<size_t, size_t> tok;
  tok = find_next_token(data, begin, end);
  bool minus(false), power(false);
  Bigfloat minus_one("-1");
  while (tok.first <= end) {
    char start = data.at(tok.first);
    if (start == '-') {
      minus = true;
      tok = find_next_token(data, tok.second + 1, end);
      continue;
    }
    if (start == '^') {
      power = true;
      tok = find_next_token(data, tok.second + 1, end);
      continue;
    }
    if (start == 'x' | start == '*' | start == '+') {
      tok = find_next_token(data, tok.second + 1, end);
      continue;
    }
    if (std::isdigit(start) || start == '.') {
      if (power) {
        power = false;
        tok = find_next_token(data, tok.second + 1, end);
        continue;
      }
      char c;
      c = data.at(tok.second + 1);
      data.at(tok.second + 1) = 0;
      coefficients.emplace_back(data.c_str() + tok.first);
      data.at(tok.second + 1) = c;
      if (minus) {
        coefficients.back() *= minus_one;
        minus = false;
      }
      tok = find_next_token(data, tok.second + 1, end);
      continue;
    }
    throw std::runtime_error(
        "Unrecognzied token in polynomial starting at " +
        data.substr(tok.first, tok.second - tok.first + 1));
  }

  Polynomial<Bigfloat> result;
  result.data().reserve(coefficients.size());
  for (auto &coeff : coefficients) {
    result.data().emplace_back(std::move(coeff));
  }
  return result;
}
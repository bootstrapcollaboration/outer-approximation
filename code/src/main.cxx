#include <boost/filesystem.hpp>
#include <boost/math/special_functions.hpp>
#include <fstream>
#include <iostream>

#include "FindMinima.hxx"
#include "DerivativeTable.hxx"

int main() {
  boost::multiprecision::mpf_float::default_precision(310);

  std::string filename("data/zzbDerivTable-d3-delta12-0-delta34-0-L0-nmax10-"
                       "keptPoleOrder20-order100.m");

  DerivativeTable dertable(filename);

  auto poly = dertable.derivative(0, 1);

  std::cout << poly << std::endl;

  // minima of sin(x^2) between 0 and 9
  // note: partitioning fails completely if you change max from 9 to 10 !
  std::cout << std::endl;
  auto minsd = getMinima([](double x){return sin(x*x);}, double(0), double(9), std::numeric_limits<double>::digits);
  for (const auto &m : minsd)
  {
        std::cout << "local minimum: x = " << m.first << ", f(" << m.first << ") = " << m.second << std::endl;
  }

  // playing with precision
  std::cout << std::endl;
  auto minsbf1 = getMinima([](Bigfloat x){return Pochhammer(x,3);}, Bigfloat(-3), Bigfloat(3),50)[0];
  auto minsbf2 = getMinima([](Bigfloat x){return Pochhammer(x,3);}, Bigfloat(-3), Bigfloat(3),100)[0];
  auto minsbf3 = getMinima([](Bigfloat x){return Pochhammer(x,3);}, Bigfloat(-3), Bigfloat(3),200)[0];
  std::cout << std::setprecision(100);
  std::cout << minsbf1.first << std::endl;
  std::cout << minsbf2.first << std::endl;
  std::cout << minsbf3.first << std::endl;
  // it's just 1/3 (-3 + Sqrt[3])
  std::cout << "-0.42264973081037423549085121949804254435239824872987312398139"
               "76735160223276970666543062846044142504748" << std::endl;

  return 0;
}
